class ContactsController < ApplicationController
  before_filter :preload_cases

  def index
    @contact = Contact.new
    @contact.entries.build

    @contacts = Contact.scoped
    if params[:tag].present?
      @contact_ids = Contact.joins(:tags).where("tags.name = ?", params[:tag]).select('"contacts"."id"').map(&:id)
      @contacts = @contacts.where(id: @contact_ids)
      raise ActiveRecord::RecordNotFound if @contacts.empty?
    end

    tag_names = Tag.all.map { |tag| "'#{tag.name}'" }
    @tags = tag_names.join(',')
  end

  def create
    @contact = Contact.new(params[:contact])

    if params[:tags]
      tag_names = params[:tags].split(',')
      tag_ids = []
      tag_names.each { |tag_name| tag_ids.push Tag.find_or_create_by_name(tag_name).id }
      @contact.tag_ids = tag_ids
    end

    if @contact.save
      redirect_to action: 'index', notice: 'Contact was successfully created.'
    else
      @contacts = Contact.all

      tag_names = Tag.all.map { |tag| "'#{tag.name}'" }
      @tags = tag_names.join(',')

      render :index
    end
  end

  def update
    @contact = Contact.find(params[:id])
    @contacts = Contact.all

    if params[:tags]
      tag_names = params[:tags].split(',')
      tag_ids = []
      tag_names.each { |tag_name| tag_ids.push Tag.find_or_create_by_name(tag_name).id }
      @contact.tag_ids = tag_ids
    end

    if @contact.update_attributes(params[:contact])
      redirect_to action: 'index', notice: 'Contact was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact = Contact.find(params[:id])
    @contact.destroy

    respond_to do |format|
      format.html { redirect_to contacts_path }
      format.json { head :no_content }
    end
  end
end
