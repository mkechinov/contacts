class AddStatusToCases < ActiveRecord::Migration
  def change
    add_column :cases, :status, :string
  end
end
