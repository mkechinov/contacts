class CreateTagsContactsJoin < ActiveRecord::Migration
  def up
  	create_table 'contacts_tags', :id => false do |t|
  		t.column 'tag_id', :integer
  		t.column 'contact_id', :integer
  	end
  end

  def down
  	drop_table 'contacts_tags'
  end
end
