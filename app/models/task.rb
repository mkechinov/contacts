class Task < ActiveRecord::Base
  attr_accessible :date,
                  :description,
                  :done,
                  :matter,
                  :priority,
                  :owner,
                  :assignee,
                  :case,
                  :assignee_id,
                  :case_id

  has_many :task_contact_associations, dependent: :destroy
  has_many :contacts, through: :task_contact_associations
  belongs_to :owner, class_name: 'User', inverse_of: :own_tasks
  belongs_to :assignee, class_name: 'User', inverse_of: :assigned_tasks
  belongs_to :case

  validates_presence_of :matter, :owner
  validates :matter, length: { maximum: 250 }

end
