# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(->
  $(".contact [class*='icon-chevron']").live('click',->
    $(@).parents('.contact').toggleClass('full-contact')
  )

  window.formState = []
  $("#add, .icon-edit").click(->
    contact = $(@).closest(".contact")
    contact.find(".visible").addClass("hidden")
    contact.find("form").removeClass("hidden")
    window.formState.push(contact.find("form").clone())
    contact.attr("form-index",window.formState.length-1)
  )

  $(".cancel").live("click",->
    contact = $(@).closest(".contact")
    contact.find("form").remove()
    contact.find(".visible").after(window.formState[contact.attr("form-index")])
    delete window.formState[contact.attr("form-index")]
    contact.removeAttr("form-index")
    contact.find("form").addClass("hidden")
    contact.find(".visible").removeClass("hidden")
  )

  $(".phone-type").live("click",->
    comment = $(@).parents('.controls-row').find('input.comment')
    comment.val($(@).text())
  )

  $('.filefield').change((e) ->
    e = e.originalEvent
    e.preventDefault()
    result = $(@).closest('.social')
    loading = window.loadImage( 
      e.target.files[0]

      (img) ->
        result.find('img').remove()
        result.prepend(img)

      noRevoke: true
    )
  )
)