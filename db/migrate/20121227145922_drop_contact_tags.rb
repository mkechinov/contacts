class DropContactTags < ActiveRecord::Migration
  def up
  	drop_table 'contacts_tags'
  end

  def down
  	create_table 'contacts_tags', id: false do |t|
  		t.column 'tag_id', :integer
  		t.column 'contact_id', :integer
  	end
  end
end
