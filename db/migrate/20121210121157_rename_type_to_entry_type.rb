class RenameTypeToEntryType < ActiveRecord::Migration
  def up
  	rename_column :contact_entries, :type, :entry_type
  end

  def down
  	rename_column :contact_entries, :entry_type, :type
  end
end
