class CreateTaskContactAssociations < ActiveRecord::Migration
  def change
    create_table :task_contact_associations do |t|
      t.integer :task_id
      t.integer :contact_id

      t.timestamps
    end
  end
end
