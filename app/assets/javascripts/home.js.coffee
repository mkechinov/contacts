# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$(->
  $('form.task').submit((e)->
    e.preventDefault()
    method = $(@).attr('method')
    action = $(@).attr('action')
    data = $(@).serialize()
    $.ajax({
      type: method,
      url: action,
      data: data
    })
  )
  window.allowSubmitting = true
  window.isChange = false
  window.TimeoutArr = []
  $('form.task').change(->
    if $(@).find('#task_matter').val() && window.allowSubmitting
      window.form = $(@)
      clearTimeout (window.TimeoutArr[$(@).attr('timeout')])
      delete window.TimeoutArr[$(@).attr('timeout')]
      window.TimeoutArr.push(
        setTimeout("window.form.submit()" ,5000)
      )
      $(@).attr("timeout",window.TimeoutArr.length-1)
  )

  $(".tray .date").click(->
    $(@).children(".datepicker").focus()
  )

  $(".datepicker").datetimepicker(
    dateFormat: "dd.mm.y;"
    beforeShow: () ->
      window.allowSubmitting = false
      return
    onSelect: (date) ->
      $(@).siblings('span.inner').html(date)
      window.isChange = true
      return
    onClose: (date, currentPicker) ->
      window.allowSubmitting = true
      if window.isChange
        currentPicker.input.parents('form.task').change()
        window.isChange = false
      return
  )

  $("[class *= 'icon-chevron']").toggle(
    ->
      $(@).removeClass().addClass('icon-chevron-up')
      $(@).parents('form').find('.additional').slideDown(300)
    ->
      $(@).removeClass().addClass('icon-chevron-down')
      $(@).parents('form').find('.additional').slideUp(300)
  )

  $(".matter").click(->
    $(@).addClass('hidden')
    $(@).siblings("input[id*='matter']").css('display', 'inline').focus()
  )

  $("input[id*='matter']").blur(->
    span = $(@).siblings('.matter')
    if span.length != 0
      span.html($(@).val())
      span.removeClass('hidden')
      $(@).css("display", "none")
  )
)