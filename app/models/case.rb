# encoding: utf-8

class Case < ActiveRecord::Base
  STATUSES = ['В процессе', 'Выиграно', 'Проиграно']
  mount_uploader :image, ImageUploader

  attr_accessible :title, :description, :status, :image, :remote_image_url
  
  has_many :business_contacts, :dependent => :destroy
  has_many :contacts, :through => :business_contacts
  has_many :tasks
  
  validates_inclusion_of :status, :in => STATUSES
  validates_presence_of :title
end


class BusinessContact < ActiveRecord::Base
  attr_accessible :case_id, :contact_id

  belongs_to :contact
  belongs_to :case
end
