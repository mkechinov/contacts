class CreateTableContactTagAssociations < ActiveRecord::Migration
  def change
	create_table 'contact_tag_associations' do |t|
		t.column 'tag_id', :integer, null: false
  		t.column 'contact_id', :integer, null: false
  	end
  end
end
