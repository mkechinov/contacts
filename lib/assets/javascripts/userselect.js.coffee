$(->
  $(document).click(->
    $(document).mousedown(->
      clearTimeout(CLICK_TIMEOUT)
      return false
    )
    CLICK_TIMEOUT = setTimeout("$(document).unbind('mousedown')",250)
  )

  $('body *').contents().each(->
    if (@.nodeType == 3)
      #Это условие на единственный дочерний элемент можно раскомментировать
      # if (@.parentElement.children.length != 0)
      $(@).wrap("<s class='user-text-select'></s>")
      if ($(@).parent()[0].innerText == '')
        $(@).parent().remove()
      # else
      #   $($(@)[0].parentElement).addClass('user-text-select')
  )
)