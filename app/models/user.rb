class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise 	:database_authenticatable,
  				:registerable,
  				:recoverable,
  				:rememberable,
  				:validatable,
  				:invitable

  has_many :own_tasks, class_name: 'Task', foreign_key: 'owner_id'
  has_many :assigned_tasks, class_name: 'Task', foreign_key: 'assignee_id'

  attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name
end
