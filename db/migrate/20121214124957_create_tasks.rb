class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :matter
      t.text :description
      t.boolean :priority
      t.datetime :date
      t.boolean :done
      t.integer :owner_id
      t.integer :assignee_id
      t.integer :case_id

      t.timestamps
    end
  end
end
