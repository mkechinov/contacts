# encoding: utf-8

class Contact < ActiveRecord::Base
  before_save :clean_data

  attr_accessible :last_name,
                  :name,
                  :tag_ids,
                  :company,
                  :info,
                  :position,
                  :entries_attributes,
                  :photo,
                  :remote_photo_url

  mount_uploader :photo, PhotoUploader

  has_many :contact_tag_associations, class_name: 'ContactTagAssociation'
  has_many :tags, through: :contact_tag_associations, dependent: :destroy
  has_many :business_contacts
  has_many :cases, through: :business_contacts, dependent: :destroy
  has_many :entries, class_name: 'ContactEntry', dependent: :destroy
  has_many :task_contact_associations
  has_many :tasks, through: :task_contact_associations, dependent: :destroy

  validates_presence_of :name
  # validates_associated :tags, :entries

  accepts_nested_attributes_for :entries, allow_destroy: true

  def clean_data
    self.name = self.name.tr(' ','')
    self.last_name = self.last_name.tr(' ','')
    self.position = self.position.strip()
    self.company = self.company.strip()
    self.info = self.info.strip()
  end

end



class EntryValidator < ActiveModel::Validator
  VALIDATION_RE = {
    email: /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/,
    phone: /^[+]?[\d]{1,3}?([\s]?\([\d]{3,6}\))?([\-\s]?[\d])*$/,
    fax: /^[+]?[\d]{1,3}?([\s]?\([\d]{3,6}\))?([\-\s]?[\d])*$/,
    skype: /^[\d\w\.\-]+$/,
    website: /^http(s)?:\/\/([\w\d\-]+\.)+[\w]{2,4}([\/][\w\d\-]+)*[\/]?$/,
    vk: /^http(s)?:\/\/([\w\d\-]+\.)*vk[\.]com[\/][\w\d\-]+$/,
    fb: /^http(s)?:\/\/(www.)?facebook[\.]com[\/][\w\d\-]+[\/]?$/,
    twttr: /^http(s)?:\/\/(www.)?twitter[\.]com[\/][\w\d\-]+[\/]?$/,
    gplus: /^http(s)?:\/\/(www.)?plus[\.]google[\.]com[\/](u\/0\/)?[\d]+([\/]|[\/]posts)?$/
  }

  def validate(record)
    unless record.entry =~ VALIDATION_RE[record.entry_type.to_sym]
      record.errors[:base] << "#{record.entry_type} entry is invalid"
    end
  end
end



class ContactEntry < ActiveRecord::Base
  TYPES = ['email', 'phone', 'fax', 'skype', 'website', 'vk', 'fb', 'twttr', 'gplus']
  #['E-mail','Телефон', 'Факс', 'Skype', 'Веб-сайт', 'Вконтакте', 'Facebook', 'Twitter', 'Google+']

  attr_accessible :entry_type, :entry, :comment
  validates_inclusion_of :entry_type, :in => TYPES
  validates_presence_of :entry_type, :entry
  validates :entry, :length => { :maximum => 150 }
  validates :comment, :length => { :maximum => 15 }
  validates_with EntryValidator
  belongs_to :contact

  scope :links, where(entry_type: ['website', 'vk', 'fb', 'twttr', 'gplus'])
  scope :not_links, where(entry_type: ['email', 'phone', 'fax', 'skype'])
end