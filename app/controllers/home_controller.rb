class HomeController < ApplicationController
  before_filter :preload_cases

  def index
    @task = Task.new(owner: current_user, assignee: current_user)
    empty_case = Case.new(title: '')
    @assigned_tasks = current_user.assigned_tasks.order('done asc').order('priority desc').order('updated_at desc').group_by { |task| task.case ? task.case : empty_case }.sort_by { |k, v| k.title }
    
    respond_to do |format|
      format.html # index.html.erb
    end
  end
end
