class TaskContactAssociation < ActiveRecord::Base
  attr_accessible :contact_id, :task_id

  belongs_to :contact
  belongs_to :task
end