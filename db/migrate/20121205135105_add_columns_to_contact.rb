class AddColumnsToContact < ActiveRecord::Migration
  def change
    add_column :contacts, :company, :string
    add_column :contacts, :info, :text
    add_column :contacts, :position, :string
  end
end
