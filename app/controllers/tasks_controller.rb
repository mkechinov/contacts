class TasksController < ApplicationController
  def create
    @task = Task.new(params[:task])
    @task.owner = current_user
    @task.contact_ids = get_contacts_by_names

    respond_to do |format|
      if @task.save
        format.html { redirect_to :back }
      else
        format.html { redirect_to :back, notice: 'Does not saved' }
      end
    end
  end

  def update
    @task = Task.find(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to :back, notice: 'Contact was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :back }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to :back }
      format.json { head :no_content }
    end
  end

  def get_contacts_by_names
    names = params[:contacts].split(',')
    contact_ids = []
    names.each do |name|
      couple_of_names = name.split(' ')
      first_name = couple_of_names[0]
      last_name = couple_of_names[1] || ''
      Contact.where({:name => first_name, :last_name => last_name}).each { |contact| contact_ids.push contact.id }
    end
    return contact_ids
  end
end