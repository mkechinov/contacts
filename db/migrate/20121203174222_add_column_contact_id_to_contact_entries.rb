class AddColumnContactIdToContactEntries < ActiveRecord::Migration
  def change
  	add_column :contact_entries, :contact_id, :integer, :null => false
  end
end
