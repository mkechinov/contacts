# encoding: utf-8

class CasesController < ApplicationController
  before_filter :preload_cases

  def show
    @case = Case.find(params[:id])
    @task = Task.new(owner: current_user, case: @case)
    @tasks = @case.tasks.order('done asc, priority desc, updated_at desc')
  end

  def new
    @case = Case.new
    @persons = construct_names_in_a_row
  end

  def edit
    @case = Case.find(params[:id])
    @persons = construct_names_in_a_row
  end

  def create
    @case = Case.new(params[:case])
    @case.contact_ids = get_contacts_by_names

    if @case.save
      redirect_to @case, notice: 'Case was successfully created.' 
    else
      render action: "new" 
    end
  end

  def update
    @case = Case.find(params[:id])
    @case.contact_ids = get_contacts_by_names

    if @case.update_attributes(params[:case])
      redirect_to @case, notice: 'Case was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @case = Case.find(params[:id])
    @case.destroy

    redirect_to cases_url
  end

  protected

  def construct_names_in_a_row
    @persons = Contact.all.collect do |contact| 
      person = "'#{"#{contact.name} #{contact.last_name}".strip}'"
    end
    @persons = "#{@persons.join(',')}"
  end

  def get_contacts_by_names
    names = params[:tags].split(',')
    contact_ids = []
    names.each do |name|
      couple_of_names = name.split(' ')
      first_name = couple_of_names[0]
      last_name = couple_of_names[1] || ''
      Contact.where({:name => first_name, :last_name => last_name}).each { |contact| contact_ids.push contact.id }
    end
    return contact_ids
  end
end
