module TasksHelper
  def task_priority_of(task)
    task.priority ? content_tag(:i, '', class: 'icon-star') : content_tag(:i, '', class: 'icon-star-empty')
  end
end