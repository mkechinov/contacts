module ContactsHelper
  def entry_icon_for(entry, options = {})
    image_tag "icons/#{entry.entry_type}.png", options
  end
end
