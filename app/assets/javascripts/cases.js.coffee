# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

$(document).ready ->

  #		Для раширения textarea в попапе при создании нового задания
  $("#comments").autoResize()

  # Подключаем tooltip
  $(".preview").tooltip()

