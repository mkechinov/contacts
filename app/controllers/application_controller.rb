class ApplicationController < ActionController::Base
  before_filter :authenticate_user!

  protect_from_forgery

  protected

  def preload_cases
  	@cases = Case.scoped.order('status asc').order('updated_at desc')
  end

end
