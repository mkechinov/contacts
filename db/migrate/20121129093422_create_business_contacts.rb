class CreateBusinessContacts < ActiveRecord::Migration
  def change
    create_table :business_contacts do |t|
      t.integer :contact_id
      t.integer :case_id

      t.timestamps
    end
  end
end
