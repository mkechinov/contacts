class Tag < ActiveRecord::Base
  attr_accessible :name,
                  :contact_ids

	has_many :contact_tag_associations, dependent: :destroy
  has_many :contacts, through: :contact_tag_associations

  validates :name,  presence: true
  validates_uniqueness_of :name

  def self.garbage_collect
  	joins(%q(LEFT OUTER JOIN "contact_tag_associations" ON "tags"."id" = "contact_tag_associations"."tag_id"))
  		.where(%q("contact_tag_associations"."tag_id" IS NULL)).destroy_all
  end
end


class ContactTagAssociation < ActiveRecord::Base
  after_destroy :clear_taglist

  attr_accessible :contact_id,
									:tag_id

	belongs_to :contact
	belongs_to :tag

  def clear_taglist
    Tag.garbage_collect
  end
end
